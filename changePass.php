<?php 
session_start();
include ("osnova.php");
require ('databaseConnection/dbConnection.php');

if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");

}
?>

<html>
<head>
<title>Промена на лозинка</title>

<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>

    <style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size:  cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>
</head>
<body>

<nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%;" data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="selectGrad.php">Почетна</a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="user.php"><span><?php if(isset($_SESSION['username']))	{						
							echo 	$_SESSION['username'] ; }
							else {
								echo "Регистрирај се";
							}
							?></span></a></li>
            <li class="has-dropdown">
					    	<a href="naracki.php" onclick = ' return naracki()'>Мои нарачки</a>
						</li>
						<li class="active"><a href="logout.php" >Logout</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>


	</nav>
 
    <br/><br/><br/><br/><br/>
    <center>
	<div  class="col-md-12 animate-box" data-animate-effect="fadeInUp">
	<h2  class="cursive-font"  style="color:white"> <b>Промена на лозинка</b></h2>
    <form action="changePass.php" method="post">
    <input type="password" name="stara" placeholder="Стара лозинка" width="150" required/><br/><br/> 
    <input type="password" name="nova1" placeholder="Нова лозинка" width="150" required/><br/><br/> 
    <input type="password" name="nova2" placeholder="Потврда на нова лозинка" width="150" required/><br/><br/><br/>
    <input type="submit" class='btn btn-danger btn-md' value="Промени ја лозинката" />
    </form>
	</div>
	</center>


     <?php 
     if(isset($_POST['stara'])) {
        $userID = $_SESSION['ID'];
        $stara = $_POST['stara'];
        $nova1 = $_POST['nova1'];
        $nova2 = $_POST['nova2'];

       
        if($nova1 != $nova2) {
            echo '<script language="javascript">';
            echo 'alert("Двете нови лозинки не се совпаѓаат!")';
            echo '</script>';
            echo '<META HTTP-EQUIV=REFRESH CONTENT="0; changePass.php">';
     } else {
            $passOld = md5($stara);
            $passNew = md5($nova1);
     }

     $query = "SELECT * FROM `clients` WHERE clientID=$userID and password='$passOld'";
     
     if (!($result = mysqli_query($dbConn, $query)))					
		echo "Грешка querу.";
  
     if($result->num_rows == 0 || $result->num_rows > 1) {
        echo '<script language="javascript">';
        echo 'alert("Погрешна лозинка!")';
        echo '</script>';
            echo '<META HTTP-EQUIV=REFRESH CONTENT="0; changePass.php">';
     } else {
         $querty = "UPDATE `clients` SET `password`='$passNew' WHERE clientID = $userID";

         if (!($resultNew = mysqli_query( $dbConn, $querty))) {				
		echo "Грешка querу1.";
         } else {
            echo '<script language="javascript">';
            echo 'alert("Промената на лозинката е успешна!")';
            echo '</script>';
   //	header("Location: login.php"); 
   echo '<META HTTP-EQUIV=REFRESH CONTENT="0; selectGrad.php">';
         }



         
     }


    }
     ?>

    <!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>
</body>
</html>