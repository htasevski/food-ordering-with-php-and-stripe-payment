<?php
session_start();
require_once ("osnova.php"); 
if(isset($_SESSION["ID"]))
{
	header("Location: selectGrad.php");

}

?>

<html> 
<head>
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

<style>

body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size:  cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>
 <script type='text/javascript'>
 function naracki () {
     alert ('Мора да се најавите пред да прегледувате нарачки');
 }
 </script>

</head>
<body>
<nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%;" data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="index.php">Почетна <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="login.php"><span><?php if(isset($_SESSION['username']))	{						
							echo 	$_SESSION['username'] ; }
							else {
								echo "Најави се";
							}
							?></span></a></li>
                         <li class="has-dropdown">
							<a href="#" onclick = ' return naracki()'>Мои нарачки</a>
							
						</li>
						<li class="active"><a href="logout.php">Logout</a></li>
						
				</div>
			</div>
			
		</div>


	</nav>
    <center>
    <br/><br/><br/><br/><br/> 
	<div class="col-md-12  animate-box" data-animate-effect="fadeInUp">
	<img src="images/chef.png" alt="chef" height="400" width="400">
        <table >
   <tr>
        <h1 style="background-color:#800000;color:white; width:79%; font-size: 3em;" class="cursive-font" >Добредојдовте на нашиот онлајн сервис за храна</h1>
        <td>
    <form action="login.php" method="get">
    <input type="submit" class="btn btn-warning" value="Нaјави се"></form>
     </td>
     <td>
    <form action="register.php" method="get">
    <input type="submit" class="btn btn-warning" value="Регистрирај се"></form>
    </td>
    </tr>
    </table>
    </div>
	</center>

    	<!-- jQuery -->
        <script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>
   
</body>
</html>