<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Moи нарачки</title> 
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">



	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
tr.noBorder td {
  border: 0;
}
</style>


</head>
<?php 
session_start();
require ('databaseConnection/dbConnection.php');

if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");

}
?>
<body>
<nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%; " data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="selectGrad.php">Почетна  </a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="user.php"><span><?php  echo 	$_SESSION['username'] ; ?></span></a></li>
						<li class="has-dropdown">
							<a href="#">Мои нарачки</a>
							
						</li>
						<li class="active"><a href="logout.php">Logout</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>
	</nav>
    <div class="gtco-section" >
		<div class="gtco-container ">
			<div class="row">
 <?php   
 $clientid = $_SESSION["ID"] ;
 

 $queryOrders = "SELECT * FROM orders WHERE userID='$clientid'";
 if (!($resultOrders = mysqli_query($dbConn, $queryOrders))) 					
 echo "Неуспешен влез во базата на податоци  ---- queryOrders";
 else{
     echo '<div style = "clear: both"></div>
     <div class = "table-responsive ">
     <table style="background-color:white;" class="table table-condensed">
     ';
    while($row = mysqli_fetch_array($resultOrders))
    {
        $restoran=$row["restoranID"];
        
        $queryRestoran  = "SELECT * FROM restaurants WHERE restoranID='$restoran'";
        if (!($resultRestoran = mysqli_query($dbConn, $queryRestoran))) 					
        echo "Неуспешен влез во базата на податоци---queryRestoranName";
        else {
            $rowRestoran = mysqli_fetch_array($resultRestoran);
        }
        echo '
        
        <tr> <td colspan="5" align="center" bgcolor=#cccccc style="color:#800000"><b>Број на нарачка:'.$row["orderID"].'</b></td></tr>
        <tr> <td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Ресторан:</b> '.$rowRestoran["restoranName"].'</td></tr>
        <tr><td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Метод на плаќање:</b> '.$row["paymentMethod"].'</td></tr>
        <tr><td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Адреса на достава:</b> '.$row["shippingAddress"].' </td></tr>
        <tr><td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Време на нарачување</b> '.$row["orderTime"].' </td></tr>
    ';
        if($row["deliveredTime"]!=null)
        echo '<tr><td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Време на пристигнување</b> '.$row["deliveredTime"].' </td></tr>
        ';
    echo '
        <tr><td  colspan="5" align="left"  style="border-bottom-style:hidden;"><b>Статус на нарачка:</b> '.$row["status"].' </td></tr>   
        <tr rowspan="2"><td  colspan="5"  align="left" style="border-bottom-style:hidden;"><b>Продукти од нарачката:</b> </td></tr>   
        <tr  ><td style="border-bottom-style:hidden;" colspan="5"></td></tr>
        ';

        
        $currentOrder=$row["orderID"];
        $queryProdukti = "SELECT products.productName, orderproducts.quantity, orderproducts.price FROM products,orderproducts WHERE orderproducts.orderID='$currentOrder' AND orderproducts.productID =products.productID";
        if (!($resultProdukti = mysqli_query($dbConn, $queryProdukti))) 					
         echo "Неуспешен влез во базата на податоци ---queryProdukti";
        else{
            while( $rowProdukti = mysqli_fetch_array($resultProdukti))
            {
                echo'
                <tr  style="border-bottom-style:hidden;">
                    <td colspan="2" align="left" >***<b>'.$rowProdukti["productName"].'</b>***</td>
                    <td colspan="2" align="left"><b>Количина:</b>'.$rowProdukti["quantity"].'</td>
                    <td colspan="1" align="left"><b>Вкупно: </b>'.$rowProdukti["price"].' ден.</td>
                </tr>
                ';
                
            }
            

        }
        echo ' <tr style="border-bottom-style:hidden;"><td colspan="4"></td>
        <td  colspan="1" align="right" ><h4 style="color:#808080"><b>Вкупно за наплата: </b>'.$row["totalPrice"].' ден.</h4> </td>
        
        </tr>
        ';

        if($row["status"]!="Доставена")
        {
        echo '
        <tr>
        <td colspan ="5" align="center">
        <form method ="post" action="potvrdiNaracka.php">
        <input type="hidden" name="hidden_id" value="'.$row['orderID'].'">
        <input type="submit"  class="btn btn-danger" name="add" value= "Потврди нарачка"/>
        </form>
        </td>
        </tr> ';
        }
        else echo '<tr><td></td></tr>';
    }
     echo'</table></div>';

 }

 

?>
         


 <br/><br/><br/>
   <div class="fh5co-text">
   <a href="selectGrad.php"><input type="submit" style=" width:100%; height:10%;  margin: auto;"  class =" btn btn-warning"
   value ="Назад">
   </a>
   </div>
             </div>
		</div>
	</div>
</body>


</html>