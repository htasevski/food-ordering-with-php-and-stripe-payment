<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Ресторани</title> 
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">



	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>


</head>

<body >

    <?php 
session_start();
require ('databaseConnection/dbConnection.php');
if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");
}
if(isset($_GET["grad"]))
		{
			$selectedGrad=$_GET["grad"];
			$_SESSION['selectedGrad'] = $selectedGrad;

		}
 else { // koga se vrakjame do nekoe od menijata 
	if((isset($_POST['grad']))||!(is_null($_POST['grad']))) { // koga od selectGrad pristapuvame
		$selectedGrad = $_POST['grad'];
		$_SESSION['selectedGrad'] = $selectedGrad;
	}
	
	
		else {
			header("Location: selectGrad.php");
		}
}

if(isset($_SESSION["cart"])&& isset($_SESSION["total"])) // ako sme se vrnale back od nekoe meni, sesijata za naracka ja brise 
{
	unset($_SESSION["cart"]);
	unset($_SESSION["total"]);
}

$restid=null;
    

$query = "select * from restaurants where restoranGrad='$selectedGrad'";
// where restoranID = $selectedRestoran
// za restorani query = " select * from restaurants where restoranGrad = $selectedGrad";

if (!($result = mysqli_query( $dbConn, $query)))					//conducting a query on the database
		echo "Грешка querу.";

    ?>

    
	<!-- <header class="page-inner">  image: url(images/img_bg_1.jpg) -->
	<nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%; " data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="selectGrad.php">Почетна  </a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="user.php"><span><?php echo 	$_SESSION['username'] ; ?></span></a></li>
						<li class="has-dropdown">
							<a href="naracki.php">Мои нарачки</a>
							
						</li>
						<li class="active"><a href="logout.php">Logout</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>


	</nav>	
	 
    
	<div class="gtco-section  animate-box ">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					 
				</div>
			</div>
			<div class="row">


<?php
 $num_rows = mysqli_num_rows($result);
 if( $num_rows ==0 )
 {
    echo '<center>
      <div class="fh5co-text" style="background-color:#800000"  >
       <p style="color:white"><span class="price cursive-font">Не се достапни ресторани од избраниот град.</span></p>
	   </div>
	   <a href="selectGrad.php"><input type="submit" style=" width:100%; height:10%;  margin: auto;"  class =" btn btn-warning" value="Назад">   </a>
  
	   </center>'; 
 }
 else{
  $openedRestaurants = 0;
  
  while($row = mysqli_fetch_array($result))
  {
    if (time() >= strtotime($row['openTime']) && time()< strtotime($row['closeTime']) )
    {
	$openedRestaurants++;
	$temp = $row['restoranID'];
    echo ' <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="'.$row['restoranID'].'" >
		<a href="menu.php?restid='.$temp.'" class="fh5co-card-item"> 
		<figure>
        <div class="overlay"><i class="ti"></i></div>
        <img src="images/'.$row['restoranID'].'.jpg" alt="Image" class="img-responsive"  style="height: 100%; width: 100%;">				
    </figure>
    <div class="fh5co-text">
    <p><span class="price cursive-font">'.$row['restoranName'] .'</span></p>
		<h2> Отворен    	</h2>
		<p>0'.$row['restoranPhone'].'</p>
		
	</div>
	</a>
	</div>';
    }

  


  }

  if($openedRestaurants==0)
  {
      echo '<center>
      <div class="fh5co-text" style="background-color:#800000" >
       <p><span class="price cursive-font">Нема отворени ресторани во моментот.</span></p>
  </div>
      </center>'; 
  }
  echo '<center>
  <div class="fh5co-text"   >
  
  <a href="selectGrad.php"><input type="submit" style=" width:100%; height:10%;  margin: auto;"  class =" btn btn-warning" value="Назад">   </a>
   
   </div>
  </center>'; 

}
     

    
  mysqli_close($dbConn);
?>

			</div>
		</div>
	</div>



	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>

</body>

</html>