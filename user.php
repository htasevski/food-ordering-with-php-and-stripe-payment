<?php 
session_start();
include ("osnova.php");
require ('databaseConnection/dbConnection.php');

if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");

}
?>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>

    <style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size:  cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>
	
    </head>
    <body>
        
    <nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%;" data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="selectGrad.php">Почетна</a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="#"><span><?php if(isset($_SESSION['username']))	{						
							echo 	$_SESSION['username'] ; }
							else {
								echo "Регистрирај се";
							}
							?></span></a></li>
            <li class="has-dropdown">
					    	<a href="naracki.php" onclick = ' return naracki()'>Мои нарачки</a>
						</li>
						<li class="active"><a href="logout.php" >Logout</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>


	</nav>

    <?php 
    $userID = $_SESSION['ID'];
    $query = "select * from clients where clientID = "."$userID";

    if (!($result = mysqli_query( $dbConn, $query)))					
		echo "Грешка querу.";


        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
				echo "
				
                <div class='col-md-12  animate-box' data-animate-effect='fadeInUp' >
                <br> <br> <br> <br> 
                <center>
		 
				<form action='changePass.php' method='get'  style='background-color:white; clear: both; width:50%;'> 
				<font size='6'>
				 
				<table style='font-family:'cursive-font'>
				<tr> <td colspan='5'   bgcolor=#cccccc  style='color:#800000; text-align:center;'><b>Податоци за корисникот</b></td></tr>
				<tr><td>
				<b>Име:</b> ".$row['name']." <br/></td></tr>
				<tr><td>
				<b>Презиме:</b> ".$row['surname']." <br/></td></tr>
				<tr><td>
				<b>Корисничко име: </b>".$row['username']." <br/></td></tr>
				<tr><td>
				<b>E-пошта:</b> ".$row['email']." <br/></td></tr>
				<tr><td>
				<b>Tелефон: </b> 0".$row['phone']."  <br/></td></tr>
				<tr><td style='text-align:center'>
				<input type='submit' class='btn btn-danger btn-md'center value='Промени лозинка'> <br/></td></tr>
				</table>
				</font>
				</form>
			 
                </center>
                </div>";
            }
        } else {
            echo "0 results";
        }

        mysqli_close($dbConn);
    ?>

    <!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>
    </body>
    </html>