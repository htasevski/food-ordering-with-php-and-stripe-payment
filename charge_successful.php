<?php
session_start();
require ("databaseConnection/dbConnection.php");
if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");
}
if(isset($_POST['stripeToken'])&&!empty($_POST['stripeToken']))
{
    $token=$_POST['stripeToken'];

    try {
        \Stripe\Charge::create(array(
            "amount" => $_SESSION["total"]*100,
            "currency" => "MKD",
            "source" => $token, // obtained with Stripe.js
            "description" => "Charge for ".$_SESSION['username']
        ));
    }
    catch(\Stripe\Error\Card $e) {
        $alert= <<<al
            <div class="alert alert-block alert-error fade in" style="text-align:center; margin-top:50px;">
                <button type="button" class="close" data-dismiss="alert">×</button>
                Грешка со вашата кредитна картичка!
            </div>
al;
    }

    $url='order.php';
    $alert= <<<al
            <div class="alert alert-block alert-success fade in" style="text-align:center; margin-top:50px;">
                <button type="button" class="close" data-dismiss="alert"> X </button>
                 Плаќањето е успешно. Ви благодариме на купувањето!
            </div>
al;
    echo '<META HTTP-EQUIV=REFRESH CONTENT="2; '.$url.'">';

 

}
else{
    header("Location: selectGrad.php");

}

?>
<html>
<header>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</header>
<body>
<center>
<div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                <img src="images/front.png" alt="Smiley face" height="400" width="400">
                    <?php echo $alert ?>
                </div>
            </div>
        </div>
</center>
</body>
</html>