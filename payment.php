<html>
<header>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Потврда на плаќање</title> 
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

		<link rel="stylesheet" href="modal.css">	
<style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size:  cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>
<style type="text/css"> 
#mydiv {
    position:absolute;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    border: 1px solid white;
    background-color: red;
	background-image : url("images/header.jpg");
}

</style>
</header>

<body>
<?php 
session_start();
require ("databaseConnection/dbConnection.php");
if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");
}
 if(isset($_POST)&&!empty($_POST))    
{
    
    $_SESSION["method"]=$_POST["method"];
    $_SESSION["address"]=$_POST["address"];
    // $_SESSION["cart"] i $_SESSION["ID"] i $_SESSION["restid"] i $_SESSION["total"]
    if ($_POST['method']=="cash")
    {
       
        header("Location: order.php");
    }
    else if ($_POST["method"]=="cart"){ ?>
     <center>
     <br/><br/><br/><br/><br/>
    
  
   
    <br/>
      <div id="mydiv" class="animate-box" data-animate-effect="fadeInUp">
     
     <table style="height:70%;">
      <tr>
        <td> <h1 class="cursive-font" ><font size ="6" color="white"> Потврда за плаќање</font> </h1> </td>
      </tr>
     
             <tr>  <br/>  <br/> 
                <td align ="center">
                    <form action="charge_successful.php" method="POST" class="payForm">
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="<?php echo $stripe_publicKey ?>"
                            data-amount="<?php echo $_SESSION["total"]*100 ?>"
                            data-name="Online Food Ordering"  ;
                            data-label="  Плати со картичка  "
                            data-id="<?php echo $_SESSION['ID'] ?>"
                            data-description="
                            <?php

                            foreach ($_SESSION["cart"] as $key => $value)  
                            {
                                    echo ''.$value["quantity"].' x '.$value['productName'].'';
                                    echo ", \r\n";
                            }

                            ?>"
                            data-currency="MKD"
                            data-locale="auto">
                        </script>
                         
                    </form>
                </td>
            </tr>
            <tr>
                <td td align ="center"><a href="menu.php?restid=<?php echo $_SESSION["restid"]; ?>" >
                <button class="btn btn-warning btn-md"> Врати се назад </button></a></td>
             </tr>
        </table>
     </tr>
     </table> 
     </center>
     </div>
     <?php
    }
    else echo "error";
}
else{
    header("Location: selectGrad.php");

}

?>



</body>
</html>