  
<?php 
session_start();
if(!isset($_SESSION["ID"]))
{
	header("Location: login.php");
}
 
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Почетна</title> 
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Bootstrap DateTimePicker -->
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">



	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
body  {
  background-image: url("images/wallpaper.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  background-attachment: fixed;  
  background-color: #cccccc;
}
</style>
<style type="text/css"> 
#mydiv {
    position:absolute;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    border: 1px solid white;
    background-color: red;
	background-image : url("images/header.jpg");
}

</style>

</head>

<body >


    
	<!-- <div class="page-inner"> -->
		<nav class="gtco-nav" role="navigation" style="background-color:#800000;width:78%; left:11%; " data-stellar-background-ratio="0.5">
		<div class="gtco-container">
			
				
		<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="selectGrad.php">Почетна </a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>

						<li class="btn-cta"><a href="user.php"><span><?php echo 	$_SESSION['username'] ; ?></span></a></li>
						<li class="has-dropdown">
							<a href="naracki.php">Мои нарачки</a>	
						</li>
						<li class="active"><a href="logout.php">Logout</a></li>
						
					</ul>	
				</div>
			</div>
			
		</div>


	</nav>
	
    <div id="mydiv" class=" animate-box"  data-animate-effect="fadeInUp">
    <center>    <p ><span class="price cursive-font" >Одбери град</span></p>  

    <form method ="post" action="showByGrad.php" style="width:20%;   margin:0 auto;">
    <select id="grad" style="margin-left:7%; " name="grad">
    <option selected="selected" value="Штип">Штип</option>
    <option value="Кочани">Кочани</option>
    <option value="Велес">Велес</option>
    <option value="Пробиштип">Пробиштип</option>
    <option value="Виница">Виница</option>
</select>
<br/>    
<br/>

    <input type = "submit"   value ="Одбери" class ="btn btn-warning" />
    </center>
    </form>
    
    </div>
          




	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>

	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	
	<script src="js/moment.min.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>


	<!-- Main -->
	<script src="js/main.js"></script>

</body>

</html>